﻿
	







Algiers house price prediction












Data source : Ouedkniss.com
Language : Python


Features: 

Features
Type
Commune
String
Etage
Integer
Superficie
Integer
Piece
Integer
Electricite
Binary
Gaz
Binary
Eau
Binary
Acte notarie
Binary
Jardin
Binary
Livret foncier
Binary
Meuble
Binary
Garage
Binary
Prix
Float


Selected features:
Commune, Etage, Superficie, Piece

Target:
Prix







Algorithms :
Algorithm
Recognition Rate
Decision Tree Regressor + Ada Boost Regressor
0.11618146309209254
Decision Tree Regressor + Bagging Regressor
0.14089356791618024
Bayesian Ridge
0.01315477774711104
K-Neighbors Regressor
0.06002152073621936
Elastic Net CV 
0.010869832826167825
Elastic Net 
0.011829166113588752
Gradient Boosting Regressor 
0.11939087333965237
Huber Regressor 
0.014384050432566187
Lasso
0.018268544396819242
Lasso CV
0.014203212651127893
Lasso Lars
0.01083257406688265
Lasso Lars CV 
0.00910628488933185
Lasso Lars IC 
0.0135400236787524
Linear Regression
0.021217915075751992
Perceptron
0.06329113924050633
Linear Regression + Polynomial Features
0.044100638120837754
Random Forest Regressor 
0.12254485863068942
Linear Regression + Shuffle
0.0336093116333287
Ridge 
0.015105453040539785
Theil Sen Regressor
0.014381140622549027





