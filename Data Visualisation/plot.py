#!/usr/bin/env python 

import pandas as pd 
import matplotlib.pyplot as plt

data = pd.read_csv("dataset.csv")

#Plot al features except Price 
for feature in data.columns:
	if feature != "Prix":
		plt.scatter(data[feature],data["Prix"], color='blue')
		plt.xlabel(feature)
		plt.ylabel('Prix')
		plt.show()