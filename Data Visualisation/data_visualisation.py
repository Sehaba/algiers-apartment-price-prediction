#!/usr/bin/env python 

import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv("dataset.csv")

#Data specification file
file = open("data_profiling.txt","w")

#For each feature 
for feature in data.columns:
	container = "##############################################################################\n#"+feature+"\n"
	container += "Length: "+str(len(data[feature])) +"\n"
	container += "Count: "+str(data[feature].count())+"\n"
	container += "Minimum: "+str(data[feature].min())+"\n"
	container += "Maximum: "+str(data[feature].max())+"\n"
	container += "Mode: "+str(data[feature].mode())+"\n"
	container += "Mean: "+str(data[feature].mean())+"\n"
	container += "Median: "+str(data[feature].median())+"\n"
	#Standard deviation 
	container += "Standard deviation: "+str(data[feature].std())+"\n"
	container += "Quantile: \n"+str(data[feature].quantile([.25, .5, .75]))+"\n"
	file.write(container)

#Create a new dataframe containing the values of ["Commune","Etage","Superficie","Piece"]
data = pd.DataFrame(data.loc[:,["Commune","Etage","Superficie","Piece","Prix"]].values)

#Calculate the correlation of the new dataframe and convert it to dataframe 
correlation = pd.DataFrame(data.corr())
#Write the correlation result in a csv file 
correlation.to_csv("correlation.csv",sep=",")